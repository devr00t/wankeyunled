#!/bin/bash
if [ ! -d "/led/" ];then
mkdir /led
cat > /led/start.sh <<EOF
#!/bin/bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH
step=1
for (( i = 0; i < 57; i=(i+step) )); do
num=$(($RANDOM%10+1 ));
if(($num  >= 7));
then
/root/led/green_on
elif (($num  >= 4 && $num <7));
then
/root/led/red_on
elif (($num  >= 1 && $num <4));
then
/root/led/blue_on
fi
sleep $step
done
exit 0
EOF
wget -qO /led/blue_off https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/blue_off
wget -qO /led/blue_on https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/blue_on
wget -qO /led/green_off https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/green_off
wget -qO /led/green_on https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/green_on
wget -qO /led/red_off https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/red_off
wget -qO /led/red_on https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/red_on
echo "*    *    *    *    *  /led/start.sh" >> /etc/crontab
echo "部署完成，请耐心等待1分钟"
echo "这期间，你可以访问一下文曦博客（www.vience.cn），看看是否有你所需要的东西"
echo "这期间，你可以访问一下我的gitee（https://gitee.com/yinjiangbi_admin），看看是否有你所需要的东西"

else
echo "貌似已经部署过了，请确认是否工作正常"
echo "如不正常，请删除根目录下led目录后重新执行，rm -rf /led"
echo "你也可以访问一下文曦博客（https://gitee.com/yinjiangbi_admin），看看是否有你所需要的东西"

fi
