#!/bin/bash
if [ ! -d "/led/" ];then
mkdir /led
wget -qO /led/blue_off https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/blue_off
wget -qO /led/blue_on https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/blue_on
wget -qO /led/green_off https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/green_off
wget -qO /led/green_on https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/green_on
wget -qO /led/red_off https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/red_off
wget -qO /led/red_on https://gitee.com/yinjiangbi_admin/wankeyunled/raw/master/red_on
chmod 777 /led/*
echo "请选择要安装的类型："
echo 
echo -e "1 - \033[36m蓝色\033[0m "
echo -e "2 - \033[36m红色\033[0m "
echo -e "3 - \033[36m绿色\033[0m "
echo
echo -n " 请选择颜色： "
read color
if [[ $color == '1' ]] ;then
echo "/led/blue_on" >> /etc/rc.local
elif [[ $color == '2' ]] ;then
echo "/led/red_on" >> /etc/rc.local
elif [[ $color == '3' ]] ;then
echo "/led/green_on" >> /etc/rc.local
else
echo "输入错误，请重新启动脚本！"
echo "你也可以访问一下文曦博客（https://gitee.com/yinjiangbi_admin），看看是否有你所需要的东西"
fi

echo "/led/blue_off" >> /etc/rc.local
echo "部署完成，请耐心等待1分钟"
echo "这期间，你可以访问一下文曦博客（www.vience.cn），看看是否有你所需要的东西"
echo "这期间，你可以访问一下我的gitee（https://gitee.com/yinjiangbi_admin），看看是否有你所需要的东西"

else
echo "貌似已经部署过了，请确认是否工作正常"
echo "如不正常，请删除根目录下led目录后重新执行，rm -rf /led"
echo "你也可以访问一下文曦博客（https://gitee.com/yinjiangbi_admin），看看是否有你所需要的东西"

fi
